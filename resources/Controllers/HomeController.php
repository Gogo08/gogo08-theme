<?php

namespace Theme\Controllers;

use Theme\Models\Post;
use Illuminate\Routing\Controller as BaseController;

class HomeController extends BaseController
{

    // Haut de page
    protected $intro;

    public function __construct(Post $model) {
      $this->intro = get_post_meta(get_the_ID(), 'th_intro', true);
    }

    public function index(Post $model) {
  	  return view('pages.home', [

        // Haut de page
        'intro' => this->intro,
      ]);
    }
}
